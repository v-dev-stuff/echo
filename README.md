# About - echo

Example deploy and test of simple 'echo' service.

It uses Auth0 to authenticate restricted APIs.  In Auth0, under `APIs`, it's the "gitlab nowsh echo service".

To get a valid JWT, use: https://jwt.io/ and use Auth0's provided secret.

# Running Locally

The source is in NodeJs, using ZEIT Now's Serverless API V2.

1. In the `/src` directory, install the dependencies if you haven't yet:


    npm install

2. Use the Now CLI to start the server in 'dev' mode:


    npm run local


# Running Integration Tests (ITs)

The ITs are in the `test` directory, and organized by type and further organized by language (for functional tests.)

One setup needed in common is to declare the `BASE_URL` environment variable. 


## JavaScript

## Python

## BlazeMeter Performance Tests
