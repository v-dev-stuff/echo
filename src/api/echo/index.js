const url = require('url');

module.exports = (request, response) => {
    let body = {
        "echo":
            {
                "path": url.parse(request.url).pathname,
                "method": request.method,
                "headers": request.headers,
                "queryParameters": request.query
            }
    };
    response.status = 200;
    response.send(body);
};
