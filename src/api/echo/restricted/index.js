const url = require('url');
const jwt = require('jsonwebtoken');

module.exports = (request, response) => {
    let verified;
    let body;
    let status = 401;
    if (request.headers.authorization) {
        let jwtSecret = process.env.JWT_SECRET;
        let token = request.headers.authorization.split(" ", 2)[1];
        verified = jwt.verify(token, jwtSecret);

        body = {
            "echo":
                {
                    "path": url.parse(request.url).pathname,
                    "method": request.method,
                    "headers": request.headers,
                    "queryParameters": request.query,
                    "jwt": verified
                }
        };
        status = 200;
    } else {
        body = {
            "code": status,
            "message": "Unauthorized"
        };
    }
    response.status(status);
    response.send(body);
};
