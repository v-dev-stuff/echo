'use strict';

const expect = require('chai').expect;
const request = require('supertest')(process.env.BASE_URL);

describe("GET", function () {
        it("base should be a GET method to echo", async function () {
            const response = await request.get("/");
            expect(response.status).to.equal(200);
            expect(response.body.echo.method).to.equal("GET");
            expect(response.body.echo.path).to.contain("/echo");
        });

        it("query should be echo'd", async function () {
            const expectedName = "hello";
            const expectedValue = "world";
            const response = await request.get(`?${expectedName}=${expectedValue}`);
            expect(response.body.echo.queryParameters).to.eql({[expectedName]: expectedValue});
        });

        it("with headers should include headers", async function () {
            const headers = {"mycustomkey": "my-value"};
            const response = await request
                .get("/")
                .set(headers);
            expect(response.body.echo.headers).includes(headers);
        });
    }
);

describe("restricted page", function () {
    const RESTRICTED = "/restricted";
    const USERNAME_ABC123_JWT = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFiYzEyMyJ9.KK-94AwSDuqiaRp2dlDrJsCjl4PcJVf2bUabGWMhKU0";
    const EXPECTED_USERNAME = "abc123";

        it("authorized should contain username jwt", async function () {
            let headers = {"Authorization": `bearer ${USERNAME_ABC123_JWT}`};
            const response = await request
                .get(RESTRICTED)
                .set(headers);
            expect(response.status).to.equal(200);
            expect(response.body.echo.method).to.equal("GET");
            expect(response.body.echo.jwt.username).to.equal(EXPECTED_USERNAME);
        });

        it("unauthorized should get 401", async function () {
            const response = await request.get(RESTRICTED);
            expect(response.status).to.equal(401);
            expect(response.body.code).to.equal(401);
            expect(response.body.message).to.equal("Unauthorized");
        });

    }
);
