'use strict';

const expect = require('chai').expect;
const request = require('supertest')(process.env.BASE_URL);

function validateMethod(response, expectedMethod) {
    expect(response.body.echo.method).to.equal(expectedMethod);
}

describe("method", function () {
        it("POST", async function () {
            const response = await request.post("/");
            validateMethod(response, "POST");
        });

        it("PUT", async function () {
            const response = await request.put("/");
            validateMethod(response, "PUT");
        });

        it("DELETE", async function () {
            const response = await request.delete("/");
            validateMethod(response, "DELETE");
        });

        it("OPTIONS", async function () {
            const response = await request.options("/");
            validateMethod(response, "OPTIONS");
        });
    }
);
