import os
from http import HTTPStatus

import requests
from assertpy import assert_that

BASE_URL = os.getenv("BASE_URL")


def test_has_get_method():
    response = requests.get(BASE_URL)
    json = response.json()
    assert_that(response.status_code).is_equal_to(HTTPStatus.OK)
    assert_that(json["echo"]["method"]).is_equal_to("GET")


def test_has_put_method():
    response = requests.put(BASE_URL)
    json = response.json()
    assert_that(json["echo"]["method"]).is_equal_to("PUT")


def test_has_delete_method():
    response = requests.delete(BASE_URL)
    json = response.json()
    assert_that(json["echo"]["method"]).is_equal_to("DELETE")


def test_has_post_method():
    response = requests.post(BASE_URL)
    json = response.json()
    assert_that(json["echo"]["method"]).is_equal_to("POST")


def test_has_query_param():
    key = "hello"
    value = "world"
    response = requests.get(f"{BASE_URL}?{key}={value}")
    json = response.json()
    assert_that(json["echo"]["queryParameters"]).is_equal_to({key: value})


def test_has_headers():
    headers = {"mykey": "myvalue"}
    response = requests.get(BASE_URL, headers=headers)
    json = response.json()
    assert_that(json["echo"]["headers"]).contains_entry(headers)


def test_has_queryparams_and_headers():
    headers = {"mykey": "myvalue"}
    key = "hello"
    value = "world"
    response = requests.get(f"{BASE_URL}?{key}={value}", headers=headers)
    json = response.json()
    assert_that(json["echo"]["queryParameters"]).is_equal_to({key: value})
    assert_that(json["echo"]["headers"]).contains_entry(headers)


def test_restricted_with_jwt():
    restricted_path = "/restricted"
    abc123_jwt = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFiYzEyMyJ9.KK-94AwSDuqiaRp2dlDrJsCjl4PcJVf2bUabGWMhKU0"
    expected_username = "abc123"

    headers = {"Authorization": f"bearer {abc123_jwt}"}
    response = requests.get(f"{BASE_URL}{restricted_path}", headers=headers)
    json = response.json()
    assert_that(response.status_code).is_equal_to(HTTPStatus.OK)
    assert_that(json["echo"]["jwt"]["username"]).is_equal_to(expected_username)


def test_restricted_without_jwt():
    restricted_path = "/restricted"
    response = requests.get(f"{BASE_URL}{restricted_path}")
    json = response.json()
    assert_that(response.status_code).is_equal_to(HTTPStatus.UNAUTHORIZED)
    assert_that(json["code"]).is_equal_to(401)
    assert_that(json["message"]).is_equal_to("Unauthorized")
